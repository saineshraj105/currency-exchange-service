package com.vsr.microservices.currencyexchangeservice;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.vsr.microservices.currencyexchangeservice.dto.CurrencyExchange;
import com.vsr.microservices.currencyexchangeservice.repository.CurrencyExchangeRepository;

@RestController
public class CurrencyExchangeController {
	private Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	Environment env;

	@Autowired
	CurrencyExchangeRepository currencyExchangeRepository;

	@GetMapping("/currency-exchange/from/{from}/to/{to}")
	public CurrencyExchange convert(@PathVariable String from, @PathVariable String to) {
		CurrencyExchange currencyExhange = currencyExchangeRepository.findByFromAndTo(from, to);
		currencyExhange.setPort(env.getProperty("server.port"));
		logger.info("currencyExhange -> {}", currencyExhange);
		return currencyExhange;
	}
}
